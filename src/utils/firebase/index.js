import * as admin from 'firebase-admin';

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: 'https://ph-covid-self-check-dev.firebaseio.com',
  projectId: 'ph-covid-self-check-dev'
});

export default admin;
