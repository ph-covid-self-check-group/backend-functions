/**
 * @param {*} body
 */
export const covidChecker = (body) => {
  const {
    travelHistoryInternational,
    travelHistoryLocal,
    exposure,
    clusterILI,
    symptoms,
    comorbitities,
    withElderly
  } = body;

  // console.log(body);

  const mildSymptomArray = [
    'fever',
    'dry-cough',
    'fatigue',
    'sputum',
    'sore-throat',
    'myalgia-arthralgia',
    'chills',
    'nausea',
    'vomiting',
    'nasal-congestion',
    'diarrhea'
  ];

  const severeSymptomArray = [
    'breathing-difficulty',
    'whistling-sound',
    'severe-condition'
  ];

  const isExposed = exposure ||
    (travelHistoryInternational && travelHistoryInternational.length > 0) ||
    (travelHistoryLocal && travelHistoryLocal.length > 0) ||
    clusterILI;

  /**
   *
   * @param {String} item
   */
  const mildSymptomsChecker = item => mildSymptomArray.indexOf(item) >= 0;

  /**
   *
   * @param {String} item
   */
  const severeSymptomChecker = item => severeSymptomArray.indexOf(item) >= 0;

  const mildSymptoms = symptoms.length > 0 && symptoms.filter(mildSymptomsChecker).length > 0;
  const severeSymptoms = symptoms.length > 0 && symptoms.filter(severeSymptomChecker).length > 0;

  const PUI = 'pui';
  const PUM = 'pum';
  const SELF_ISOLATION = 'self-isolation';
  const GOOD_HYGIENE = 'good-hygiene';
  const CONTACT_HOSPITAL = 'contact-hospital';
  const SPECIAL_INSTRUCTIONS_PPE = 'special-instructions-ppe';
  const HOME_QUARANTINE = 'home-quarantine';
  const REFER_TO_ER = 'refer-to-er';
  const OBSERVE_SYMPTOMS = 'observe-symptoms';
  const LOW = 'low';
  const MEDIUM = 'medium';
  const HIGH = 'high';

  /**
   * @type {Array<String>}
   */
  let actions = [];
  let severity = LOW;

  // console.log(isExposed, mildSymptoms, severeSymptoms, clusterILI, comorbitities)

  if (!isExposed) {
    if (!mildSymptoms && !severeSymptoms) {
      actions = [SELF_ISOLATION, GOOD_HYGIENE];
    } else {
      if (clusterILI) {
        if (severeSymptoms) {
          actions = [PUI, CONTACT_HOSPITAL, SPECIAL_INSTRUCTIONS_PPE];
          severity = HIGH;
        } else if (mildSymptoms) {
          if (comorbitities || withElderly) {
            actions = [PUI, CONTACT_HOSPITAL, SPECIAL_INSTRUCTIONS_PPE];
            severity = HIGH;
          } else {
            actions = [PUI, HOME_QUARANTINE];
            severity = MEDIUM;
          }
        }
      } else if (severeSymptoms) {
        actions = [PUI, CONTACT_HOSPITAL, SPECIAL_INSTRUCTIONS_PPE];
        severity = HIGH;
      } else {
        actions = [REFER_TO_ER];
        severity = HIGH;
      }
    }
  } else {
    if (!mildSymptoms && !severeSymptoms) {
      actions = [PUM, OBSERVE_SYMPTOMS, HOME_QUARANTINE];
      severity = MEDIUM;
    } else {
      if (mildSymptoms && !severeSymptoms) {
        if (comorbitities || withElderly) {
          actions = [PUI, CONTACT_HOSPITAL, SPECIAL_INSTRUCTIONS_PPE];
          severity = HIGH;
        } else {
          actions = [PUI, HOME_QUARANTINE];
          severity = MEDIUM;
        }
      } else if (severeSymptoms) {
        actions = [PUI, CONTACT_HOSPITAL, SPECIAL_INSTRUCTIONS_PPE];
        severity = HIGH;
      }
    }
  }

  // console.log(actions);

  return { actions, severity };
};
