export const baseResponseTemplate = {
  400: {
    description: 'Bad request',
    type: 'object',
    properties: {
      success: { type: 'boolean', value: false, default: false },
      code: { type: 'string', value: 'request/malformed' },
      message: {
        type: 'string',
        value: 'The request could not be understood by the server due to malformed syntax. The client SHOULD NOT repeat the request without modifications.'
      }
    }
  },
  401: {
    description: 'Unauthorized',
    type: 'object',
    properties: {
      success: { type: 'boolean', value: false, default: false },
      code: { type: 'string', value: 'auth/unauthorized' },
      message: {
        type: 'string',
        value: 'The request requires user authentication. The client MAY repeat the request with a suitable Authorization header field (section 14.8). If the request already included Authorization credentials, then the 401 response indicates that authorization has been refused for those credentials.'
      }
    }
  },
  403: {
    description: 'Forbidden',
    type: 'object',
    properties: {
      success: { type: 'boolean', value: false, default: false },
      code: { type: 'string', value: 'auth/forbidden' },
      message: {
        type: 'string',
        value: 'The server understood the request, but is refusing to fulfill it. Authorization will not help and the request SHOULD NOT be repeated.'
      }
    }
  },
  404: {
    description: 'Not Found',
    type: 'object',
    properties: {
      success: { type: 'boolean', value: false, default: false },
      code: { type: 'string', value: 'data/not-found' },
      message: {
        type: 'string',
        value: 'The data you are trying to find cannot be found'
      }
    }
  },
  409: {
    description: 'Conflict',
    type: 'object',
    properties: {
      success: { type: 'boolean', value: false, default: false },
      code: { type: 'string', value: 'request/conflict' },
      message: {
        type: 'string',
        value: 'The request could not be completed due to a conflict with the current state of the resource. This code is only allowed in situations where it is expected that the user might be able to resolve the conflict and resubmit the request.'
      }
    }
  },
  429: {
    description: 'Too many requests',
    type: 'object',
    properties: {
      success: { type: 'boolean', value: false, default: false },
      code: { type: 'string', value: 'request/rate-limit' },
      message: {
        type: 'string',
        value: 'The user has sent too many requests in a given amount of time ("rate limiting")'
      }
    }
  },
  500: {
    description: 'Internal Server Error',
    type: 'object',
    properties: {
      success: { type: 'boolean', value: false, default: false },
      code: { type: 'string', value: 'server/general-error' },
      message: {
        type: 'string',
        value: 'The server encountered an unexpected condition which prevented it from fulfilling the request.'
      }
    }
  },
  503: {
    description: 'Server Unavailable',
    type: 'object',
    properties: {
      success: { type: 'boolean', value: false, default: false },
      code: { type: 'string', value: 'server/unavailable' },
      message: {
        type: 'string',
        value: 'The server is currently unable to handle the request due to a temporary overloading or maintenance of the server.'
      }
    }
  }
};
