import admin from '../firebase';

const db = admin.database();

/**
 *
 * @param {string} email
 * @return {string}
 */
export const emailToUid = (email) => {
  return email.replace(/\./g, '_').replace(/@/g, '(at)');
};

/**
 *
 * @param {*} req
 * @param {*} res
 */
export const verify = async (req, res) => {
  const [, token] = (req.headers && req.headers.authorization && req.headers.authorization.split('Bearer ')) || [null, null];

  if (!token) {
    res.unauthorized('Malformed authorization header');
    return;
  }

  try {
    const decodedToken = await admin.auth().verifyIdToken(token);
    const { email } = decodedToken;

    const user = await db.ref(`/users/data/${emailToUid(email)}`).once('value');

    if (!user.val()) {
      res.forbidden();
      return;
    }

    return { success: true };
  } catch (error) {
    console.error(error);
    res.unauthorized(error.message);
    return null;
  }
};

/**
 *
 * @param {*} req
 * @param {*} res
 */
export const verifyAdmin = async (req, res) => {
  const [, token] = (req.headers && req.headers.authorization && req.headers.authorization.split('Bearer ')) || [null, null];

  if (!token) {
    res.unauthorized('Malformed authorization header');
    return;
  }

  try {
    const decodedToken = await admin.auth().verifyIdToken(token);
    const { email } = decodedToken;

    const snapshot = await db.ref(`/users/data/${emailToUid(email)}`).once('value');
    const user = snapshot.val();

    if (!user || !user.admin) {
      res.forbidden();
      return;
    }

    return { success: true };
  } catch (error) {
    console.error(error);
    res.unauthorized(error.message);
    return null;
  }
};
