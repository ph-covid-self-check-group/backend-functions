export const Patient = {
  $id: 'Patient',
  type: 'object',
  required: [
    'pid',
    'firstname',
    'middlename',
    'lastname',
    'status',
    'gender',
    'occupation',
    'address',
    'barangay',
    'city',
    'province',
    'birthdate',
    'age',
    'homenumber',
    'mobilenumber',
    'symptoms',
    'travelHistoryInternational',
    'travelHistoryLocal',
    'exposure',
    'comorbitities',
    'clusterILI',
    'withElderly',
    'email'
  ],
  properties: {
    pid: {
      type: 'string',
      description: 'Unique Identifier, can be used as case id'
    },
    patientNumber: {
      type: 'string',
      description: 'Unique patient number'
    },
    firstname: {
      type: 'string',
      description: 'First name of the patient'
    },
    middlename: {
      type: 'string',
      description: 'Middle name of the patient'
    },
    lastname: {
      type: 'string',
      description: 'Last name of the patient'
    },
    status: {
      type: 'string',
      enum: [
        'single',
        'divorce',
        'annulled',
        'widow',
        'married'
      ],
      description: 'Status of the patient'
    },
    gender: {
      type: 'string',
      enum: [
        'male',
        'female',
        'non-binary',
        'non-disclosed'
      ],
      description: 'Gender of the patient'
    },
    email: {
      type: 'string',
      format: 'email',
      description: 'Email of the patient'
    },
    occupation: {
      type: 'string',
      description: 'Occupation of the patient'
    },
    address: {
      type: 'string',
      description: 'Home # or Street address of the patient'
    },
    barangay: {
      type: 'string',
      description: 'Barangay where the patient lives'
    },
    city: {
      type: 'string',
      description: 'City/Town where the patient lives'
    },
    province: {
      type: 'string',
      description: 'Province where the patient lives'
    },
    birthdate: {
      type: 'string',
      description: 'Birthdate of the patient in YYYY-MM-DD format'
    },
    age: {
      type: 'number',
      description: 'Age of the patient'
    },
    homenumber: {
      type: 'string',
      description: 'Telephone (Home) Number of the patient'
    },
    mobilenumber: {
      type: 'string',
      description: 'Mobile Number of the patient'
    },
    symptoms: {
      type: 'array',
      items: {
        type: 'string',
        enum: [
          'fever',
          'dry-cough',
          'fatigue',
          'sputum',
          'sore-throat',
          'headache',
          'myalgia-arthralgia',
          'chills',
          'nausea',
          'vomiting',
          'nasal-congestion',
          'diarrhea',
          'breathing-difficulty',
          'whistling-sound',
          'severe-condition'
        ]
      },
      description: 'Array of symptoms of that are being tracked for Covid'
    },
    travelHistoryInternational: {
      type: 'array',
      items: { type: 'string' },
      description: 'Travel history of the patient (International), should be in format "City/Town, Country"'
    },
    travelHistoryLocal: {
      type: 'array',
      items: { type: 'string' },
      description: 'Travel history of the patient (Local), should be in format "City/Town, Province"'
    },
    severity: {
      type: 'string',
      enum: [
        'low',
        'medium',
        'high'
      ],
      description: 'Risk factor of the patient given symptoms'
    },
    actions: {
      type: 'array',
      items: {
        type: 'string',
        enum: [
          'pum',
          'pui',
          'contact-hospital',
          'special-instrcutions-ppe',
          'home-quarantine',
          'observe-symptoms',
          'refer-to-er',
          'self-isolation',
          'good-hygiene',
          'test-positive'
        ]
      },
      description: 'Actions need to be taken by the patient based on the symptoms.'
    },
    exposure: {
      type: 'boolean',
      description: 'If the patient has been exposed to a person who is a PUI or PUM or confirmed case of Covid-19'
    },
    clusterILI: {
      type: 'boolean',
      description: 'If the patient is living or working in a group or cluster with influenza like symptoms'
    },
    comorbitities: {
      type: 'boolean',
      description: 'If the patient has been existing comorbitities.'
    },
    withElderly: {
      type: 'boolean',
      description: 'If the patient is living with elderly'
    },
    createdAt: {
      type: 'number',
      description: 'Created time of the patient in millisecond precision.'
    },
    updatedAt: {
      type: 'number',
      description: 'Updated time of the patient in millisecond precision.'
    },
    forReview: {
      type: 'boolean',
      description: 'If the patient needs to be reviewed'
    },
    correctRecommendation: {
      type: 'boolean',
      description: 'If the system\'s recommendation and risk factor is correct as checked by a healthworker'
    },
    caseClosed: {
      type: 'boolean',
      description: 'If the patient\'s file has been closed'
    },
    healthWorkerAction: {
      type: 'array',
      items: {
        type: 'string',
        enum: [
          'call',
          'text',
          'email'
        ]
      },
      description: 'Healthworker\'s action towards the case'
    }
  }
};

export const PatientUpdateRequest = {
  $id: 'PatientUpdateRequest',
  type: 'object',
  required: [],
  properties: {
    ...Patient.properties
  }
};

delete PatientUpdateRequest.properties.pid;
delete PatientUpdateRequest.properties.patientNumber;
delete PatientUpdateRequest.properties.actions;
delete PatientUpdateRequest.properties.createdAt;
delete PatientUpdateRequest.properties.updatedAt;

// delete PatientUpdateRequest.properties.caseClosed;
// delete PatientUpdateRequest.properties.correctRecommendation;
// delete PatientUpdateRequest.properties.healthWorkerAction;

export const PatientUpdateRequestMany = {
  $id: 'PatientUpdateRequestMany',
  type: 'object',
  required: [],
  properties: {
    ...Patient.properties
  }
};

delete PatientUpdateRequestMany.properties.actions;
delete PatientUpdateRequestMany.properties.createdAt;
delete PatientUpdateRequestMany.properties.updatedAt;
delete PatientUpdateRequestMany.properties.caseClosed;
delete PatientUpdateRequestMany.properties.correctRecommendation;
delete PatientUpdateRequestMany.properties.healthWorkerAction;

export const PatientUpdateResponse = {
  $id: 'PatientUpdateResponse',
  type: 'object',
  required: [
    'pid'
  ],
  properties: {
    ...Patient.properties
  }
};

export const PatientCreateRequest = {
  $id: 'PatientCreateRequest',
  type: 'object',
  required: [
    'firstname',
    'lastname',
    'status',
    'gender',
    'city',
    'province',
    'birthdate',
    'age',
    'mobilenumber',
    'symptoms',
    'travelHistoryInternational',
    'travelHistoryLocal',
    'exposure',
    'comorbitities',
    'clusterILI',
    'withElderly',
    'email'
  ],
  properties: {
    ...Patient.properties
  }
};

delete PatientCreateRequest.properties.pid;
delete PatientCreateRequest.properties.patientNumber;
delete PatientCreateRequest.properties.actions;
delete PatientCreateRequest.properties.createdAt;
delete PatientCreateRequest.properties.updatedAt;
delete PatientCreateRequest.properties.caseClosed;
delete PatientCreateRequest.properties.correctRecommendation;
delete PatientCreateRequest.properties.healthWorkerAction;
delete PatientCreateRequest.properties.severity;
delete PatientCreateRequest.properties.forReview;

export default { Patient, PatientUpdateRequest, PatientUpdateResponse, PatientCreateRequest };
