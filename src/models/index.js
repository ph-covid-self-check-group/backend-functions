import { Patient, PatientUpdateRequest, PatientUpdateResponse, PatientCreateRequest, PatientUpdateRequestMany } from './patient';
import { User, UserCreateRequest, UserUpdateRequest, UserUpdateRequestMany, UserUpdateResponse } from './user';
import { RecommendationRequest } from './recommendation';

/**
* @type {SwaggerModelDefinition}
*/
export const definitions = {
  Patient,
  PatientUpdateRequest,
  PatientUpdateRequestMany,
  PatientUpdateResponse,
  PatientCreateRequest,
  RecommendationRequest,
  User,
  UserCreateRequest,
  UserUpdateRequest,
  UserUpdateRequestMany,
  UserUpdateResponse
};
