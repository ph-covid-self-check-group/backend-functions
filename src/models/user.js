export const User = {
  $id: 'User',
  type: 'object',
  required: [
    'uid',
    'admin',
    'email'
  ],
  properties: {
    uid: {
      type: 'string',
      description: 'Unique Identifier, can be used as case id'
    },
    fullname: {
      type: 'string',
      description: 'Fullname of user'
    },
    email: {
      type: 'string',
      description: 'Email',
      format: 'email'
    },
    admin: {
      type: 'boolean',
      description: 'Is Admin?'
    }
  }
};

export const UserUpdateRequest = {
  $id: 'UserUpdateRequest',
  type: 'object',
  required: [],
  properties: {
    ...User.properties
  }
};

delete UserUpdateRequest.properties.uid;
delete UserUpdateRequest.properties.email;

export const UserUpdateRequestMany = {
  $id: 'UserUpdateRequestMany',
  type: 'object',
  required: [],
  properties: {
    ...User.properties
  }
};

delete UserUpdateRequestMany.properties.email;

export const UserUpdateResponse = {
  $id: 'UserUpdateResponse',
  type: 'object',
  required: [
    'pid'
  ],
  properties: {
    ...User.properties
  }
};

export const UserCreateRequest = {
  $id: 'UserCreateRequest',
  type: 'object',
  required: [
    'admin',
    'email'
  ],
  properties: {
    ...User.properties
  }
};

delete UserCreateRequest.properties.uid;
// delete UserCreateRequest.properties.fullname;

export default { User, UserUpdateRequest, UserUpdateResponse, UserCreateRequest };
