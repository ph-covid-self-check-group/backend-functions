import { Patient } from './patient';

const {
  symptoms,
  travelHistoryInternational,
  travelHistoryLocal,
  exposure,
  comorbitities,
  clusterILI,
  withElderly
} = Patient.properties;

/**
 * @type {*}
 */
export const RecommendationRequest = {
  $id: 'RecommendationRequest',
  type: 'object',
  required: [
    'symptoms',
    'travelHistoryInternational',
    'travelHistoryLocal',
    'exposure',
    'comorbitities',
    'clusterILI',
    'withElderly'
  ],
  properties: {
    symptoms,
    travelHistoryInternational,
    travelHistoryLocal,
    exposure,
    comorbitities,
    clusterILI,
    withElderly
  }
};

export default { RecommendationRequest };
