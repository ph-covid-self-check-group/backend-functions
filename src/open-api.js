import { definitions } from './models';
import { version } from '../package.json';
import securityDefinitions from './security.json';

const schemes = [];

if (process.env.NODE_ENV !== 'production') {
  schemes.push('http');
} else {
  schemes.push('https');
}

export const swagger = {
  info: {
    title: 'PH Covid Self Check Backend',
    description: 'testing the fastify swagger api',
    version
  },
  schemes,
  consumes: ['application/json'],
  produces: ['application/json'],
  definitions,
  securityDefinitions
};
