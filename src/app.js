// @ts-ignore
import Fastify from 'fastify';
// @ts-ignore
import fastifySwagger from 'fastify-swagger';
import { swagger } from './open-api';
import { api } from './api';

const fastify = Fastify({
  logger: true,
  trustProxy: true
});

fastify.register(require('fastify-cors'), {
  origin: true
});

fastify.register(require('fastify-sensible'));

fastify.register(fastifySwagger, {
  routePrefix: '/docs',
  swagger,
  exposeRoute: true
});

for (const models in swagger.definitions) {
  fastify.addSchema(swagger.definitions[models]);
}

for (const key in api) {
  for (const path in api[key]) {
    for (const method in api[key][path]) {
      const { schema, fn } = api[key][path][method];
      // @ts-ignore
      if (fastify[method] && typeof fastify[method] === 'function') {
        // @ts-ignore
        fastify[method](`${schema.api ? `/api${path}` : path}`, { schema }, fn);
      }
    }
  }
}

export default fastify;
