import { fn } from './fn.js';

export const get = {
  schema: {
    description: 'Endpoint for Server Health Checks',
    tags: ['Server Health'],
    summary: 'Endpoint for Server Health checks',
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' }
        }
      }
    }
  },
  fn
};
