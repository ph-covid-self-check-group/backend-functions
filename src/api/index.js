import { health } from './health';
import { patient } from './patient';
import { user } from './user';
import { recommendation } from './recommendation';
import { auth } from './auth';
import { authAdmin } from './auth-admin';

/**
* @type {{ [key: string]: { [key: string]: { [key: string]: { schema: object, fn: function } } } }}
*/
export const api = {
  health,
  patient,
  user,
  recommendation,
  auth,
  authAdmin
};
