import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';
import { postResponseTemplate } from '../../../utils/response-templates/post-response-template';

export const del = {
  schema: {
    api: true,
    description: 'Delete one Patient object',
    tags: ['Patient'],
    summary: 'This method allows you to delete one Patient object',
    params: {
      type: 'object',
      properties: {
        pid: { type: 'string' }
      }
    },
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          patient: 'PatientUpdateResponse#'
        }
      },
      ...baseResponseTemplate,
      ...postResponseTemplate
    }
  },
  fn
};
