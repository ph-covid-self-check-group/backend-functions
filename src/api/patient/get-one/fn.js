
import admin from '../../../utils/firebase';
import { verify } from '../../../utils/auth/verify';

const db = admin.firestore();

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply<Response>} res
 */
export const fn = async (req, res) => {
  const result = await verify(req, res);

  if (!result) {
    return;
  }

  const { params } = req;
  const { pid } = params;

  const ref = db.collection('patients').doc(pid);
  const doc = await ref.get();

  if (!doc.exists) {
    return res
      .code(404)
      .send({
        success: false,
        code: 'data/not-found',
        message: 'The data you are trying to find cannot be found'
      });
  }

  const obj = doc.data() || {};

  return {
    success: true,
    data: {
      ...obj,
      createdAt: obj.createdAt.toDate().getTime(),
      updatedAt: obj.updatedAt.toDate().getTime(),
      pid: doc.id
    }
  };
};
