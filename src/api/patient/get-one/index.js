import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';

export const get = {
  schema: {
    api: true,
    description: 'Get one Patient object',
    tags: ['Patient'],
    summary: 'This method allows you to get one Patient object',
    params: {
      type: 'object',
      properties: {
        pid: { type: 'string' }
      }
    },
    security: [
      {
        apiKey: []
      }
    ],
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          data: 'PatientUpdateResponse#'
        }
      },
      ...baseResponseTemplate
    }
  },
  fn
};
