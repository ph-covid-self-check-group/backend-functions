// import admin from '../../../utils/firebase';
import { verify } from '../../../utils/auth/verify';

// const db = admin.firestore();

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply<Response>} res
 */
export const fn = async (req, res) => {
  const result = await verify(req, res);
  if (!result) return;

  // const { body: patients } = req;
  // const batch = db.batch();
  // patients.forEach((patient) => {
  //   batch.delete(db.doc(`patients/${patient}`));
  // });
  // await batch.commit();
  return { success: false };
};
