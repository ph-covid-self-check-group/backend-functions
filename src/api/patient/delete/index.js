import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';
import { postResponseTemplate } from '../../../utils/response-templates/post-response-template';

export const del = {
  schema: {
    api: true,
    description: 'Deletes a list of Patient objects',
    tags: ['Patient'],
    summary: 'This method allows you to delete a list of Patient objects',
    body: {
      type: 'array',
      items: {
        type: 'string'
      }
    },
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          items: {
            type: 'array',
            items: { $ref: 'PatientUpdateResponse#' }
          }
        }
      },
      ...baseResponseTemplate,
      ...postResponseTemplate
    }
  },
  fn
};
