import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';
import { postResponseTemplate } from '../../../utils/response-templates/post-response-template';

export const post = {
  schema: {
    api: true,
    description: 'Creates at least one Patient object',
    tags: ['Patient'],
    summary: 'This method allows you to create at least Patient object. Use this also for batch create.',
    body: {
      type: 'array',
      items: 'PatientCreateRequest#'
    },
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          items: {
            type: 'array',
            items: { $ref: 'PatientUpdateResponse#' }
          }
        }
      },
      ...baseResponseTemplate,
      ...postResponseTemplate
    }
  },
  fn
};
