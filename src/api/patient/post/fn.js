import admin from '../../../utils/firebase';
import { covidChecker } from '../../../utils/covid-checker';
import Chance from 'chance';

const chance = new Chance();
const db = admin.firestore();
const { FieldValue } = admin.firestore;

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply<Response>} res
 */
export const fn = async (req, res) => {
  if (req.body.length > 1) {
    return res.payloadTooLarge();
  } else if (req.body.length < 1) {
    return res.badRequest();
  }

  const item = req.body[0];
  const { actions, severity } = covidChecker(item);

  let patientNumber;
  let docs;

  do {
    patientNumber = chance.string({ length: 3, casing: 'upper', alpha: true }) + chance.string({ length: 5, numeric: true });
    docs = await db.collection('patients').where('patientNumber', '==', patientNumber).get();
  } while (docs.size > 0);

  const ref = await db.collection('patients').add({
    ...item,
    patientNumber,
    actions,
    severity,
    correctRecommendation: false,
    healthWorkerAction: [],
    caseClosed: false,
    createdAt: FieldValue.serverTimestamp(),
    updatedAt: FieldValue.serverTimestamp()
  });

  const items = [{ pid: ref.id, ...req.body[0], actions, severity, patientNumber }];

  return { success: true, items };
};
