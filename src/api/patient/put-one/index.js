import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';
import { postResponseTemplate } from '../../../utils/response-templates/post-response-template';

export const put = {
  schema: {
    api: true,
    description: 'Update one Patient object',
    tags: ['Patient'],
    summary: 'This method allows you to update one Patient object',
    params: {
      type: 'object',
      properties: {
        pid: { type: 'string' }
      }
    },
    body: {
      type: 'object',
      properties: {
        data: 'PatientUpdateRequest#'
      }
    },
    security: [
      {
        apiKey: []
      }
    ],
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          data: 'PatientUpdateResponse#'
        }
      },
      ...baseResponseTemplate,
      ...postResponseTemplate
    }
  },
  fn
};
