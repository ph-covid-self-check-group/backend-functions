import admin from '../../../utils/firebase';
// import { verify } from '../../../utils/auth/verify';

const db = admin.firestore();
const FieldValue = admin.firestore.FieldValue;

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply<Response>} res
 */
export const fn = async (req, res) => {
  // const result = await verify(req, res);
  // if (!result) return;

  const { body: patients } = req;
  if (patients.length === 0) {
    return res.badRequest();
  }

  const batch = db.batch();
  patients.forEach((patient) => {
    patient.updatedAt = FieldValue.serverTimestamp();
    batch.set(db.doc(`patients/${patient.pid}`), patient, { merge: true });
  });
  await batch.commit();

  const snapshot = await db.collection('patients').where('pid', 'in', patients.map((x) => x.pid)).get();
  const items = snapshot.docs.map((doc) => doc.data());
  return { success: true, items };
};
