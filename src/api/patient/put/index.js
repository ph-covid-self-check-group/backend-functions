import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';
import { postResponseTemplate } from '../../../utils/response-templates/post-response-template';

export const put = {
  schema: {
    api: true,
    description: 'Updates a list of Patient objects',
    tags: ['Patient'],
    summary: 'This method allows you to update a list of Patient objects',
    body: {
      type: 'array',
      items: 'PatientUpdateRequestMany#'
    },
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          items: {
            type: 'array',
            items: { $ref: 'PatientUpdateResponse#' }
          }
        }
      },
      ...baseResponseTemplate,
      ...postResponseTemplate
    }
  },
  fn
};
