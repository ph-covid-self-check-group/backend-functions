// import { del } from './delete';
// import { del as deleteOne } from './delete-one';
import { get } from './get';
import { get as getOne } from './get-one';
import { post } from './post';
// import { put } from './put';
import { put as putOne } from './put-one';

export const patient = {
  '/patient/:pid': {
    // delete: deleteOne,
    get: getOne,
    put: putOne
  },
  '/patient': {
    // delete: del,
    get,
    post
    // put
  }
};
