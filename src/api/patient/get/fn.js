import admin from '../../../utils/firebase';
import { verify } from '../../../utils/auth/verify';

const db = admin.firestore();

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply<Response>} res
 */
export const fn = async (req, res) => {
  const result = await verify(req, res);

  if (!result) {
    return;
  }

  const { query } = req;
  let limit = 50;
  let startAt;
  let endAt;
  let caseClosed = null;
  let status = null;
  if (query) {
    limit = query.limit && parseInt(query.limit) <= 100 ? parseInt(query.limit) : 50;
    startAt = query.startAt;
    endAt = query.endAt;
    if (query.caseClosed === 'true') {
      caseClosed = true;
    } else if (query.caseClosed === 'false') {
      caseClosed = false;
    }

    status = query.status || null;
  }

  /**
   * @type {Array<any>}
   */
  const items = [];

  const collection = db.collection('patients');
  let docSnapshot = { exists: false };
  if (startAt) {
    docSnapshot = await collection.doc(startAt).get();
  } else if (endAt) {
    docSnapshot = await collection.doc(endAt).get();
  }
  let ref = collection.orderBy('createdAt', 'desc');

  // console.log(status);

  ref = caseClosed !== null ? ref.where('caseClosed', '==', caseClosed) : ref;
  ref = status ? ref.where('actions', 'array-contains', status) : ref;

  let snapshot = null;

  if (docSnapshot && docSnapshot.exists) {
    if (startAt) {
      snapshot = await ref.limit(limit).startAfter(docSnapshot).get();
    } else if (endAt) {
      snapshot = await ref.limit(limit).endBefore(docSnapshot).get();
    }
  } else {
    snapshot = await ref.limit(limit).get();
  }

  if (snapshot) {
    snapshot.forEach(doc => {
      items.push({ pid: doc.id, ...doc.data() });
    });
  }

  // TODO: Don't use this on production, use SQL
  const snap = await ref.get();

  return {
    success: true,
    items: items.map(item => ({
      ...item,
      createdAt: item.createdAt.toDate().getTime(),
      updatedAt: item.updatedAt.toDate().getTime()
    })),
    count: snap.size
  };
};
