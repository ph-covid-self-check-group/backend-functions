import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';

export const get = {
  schema: {
    api: true,
    description: 'Get a list of Patient objects',
    tags: ['Patient'],
    summary: 'This method allows you to get Patient objects',
    query: {
      limit: {
        type: 'number'
      },
      startAt: {
        type: 'string',
        description: 'Get the list starting from the last Patient ID of the last request made'
      },
      endAt: {
        type: 'string',
        description: 'Get the list before the first Patient ID of the last request made'
      },
      caseClosed: {
        type: 'string',
        enum: ['true', 'false', ''],
        description: 'false if it is still being reviewed, true if it has been reviewed'
      },
      status: {
        type: 'string',
        enum: ['pui', 'pum', 'test-positive'],
        description: 'Add a filter to check if the patient is a PUI, PUM, or test-positive'
      }
    },
    security: [
      {
        apiKey: []
      }
    ],
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          items: {
            type: 'array',
            items: { $ref: 'PatientUpdateResponse#' }
          },
          count: {
            type: 'number'
          }
        }
      },
      ...baseResponseTemplate
    }
  },
  fn
};
