/**
 * @license
 * Copyright 2020, PH Covid Self Check Volunteer Group.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { covidChecker } from '../../../utils/covid-checker';

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply<Response>} res
 */
export const fn = async (req, res) => {
  const { body } = req;

  if (!body.data) {
    return res
      .code(400)
      .send({
        success: false,
        code: 'request/malformed',
        message: 'The request could not be understood by the server due to malformed syntax. The client SHOULD NOT repeat the request without modifications.'
      });
  }

  const { actions, severity } = covidChecker(body.data);

  return {
    success: true,
    data: {
      actions,
      severity
    }
  };
};
