/**
 * @license
 * Copyright 2020, PH Covid Self Check Volunteer Group.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';
import { postResponseTemplate } from '../../../utils/response-templates/post-response-template';

export const post = {
  schema: {
    api: true,
    description: 'This gives a recommendation based on the symptoms given',
    tags: ['Recommendation'],
    summary: 'This action method gives a recommendation based on the symptoms needed',
    body: {
      type: 'object',
      properties: {
        data: 'RecommendationRequest#'
      }
    },
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          data: {
            type: 'object',
            properties: {
              actions: {
                type: 'array',
                items: {
                  type: 'string'
                }
              }
            }
          }
        }
      },
      ...baseResponseTemplate,
      ...postResponseTemplate
    }
  },
  fn
};
