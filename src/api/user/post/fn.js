import admin from '../../../utils/firebase';
import { emailToUid, verifyAdmin } from '../../../utils/auth/verify';
const db = admin.database();

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply<Response>} res
 */
export const fn = async (req, res) => {
  const result = await verifyAdmin(req, res);

  if (!result) {
    return;
  }

  if (req.body.length < 1) {
    return res.badRequest();
  }

  const items = [];

  for await (const item of req.body) {
    await db.ref(`/users/data/${emailToUid(item.email)}`).set({
      fullname: item.fullname || item.email,
      email: item.email,
      admin: item.admin || false
    });

    items.push({
      uid: emailToUid(item.email),
      email: item.email,
      admin: item.admin || false
    });
  }

  return { success: true, items };
};
