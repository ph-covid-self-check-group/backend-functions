import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';

export const get = {
  schema: {
    api: true,
    description: 'Get a list of User objects',
    tags: ['User'],
    summary: 'This method allows you to get User objects',
    security: [
      {
        apiKey: []
      }
    ],
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          items: {
            type: 'array',
            items: { $ref: 'UserUpdateResponse#' }
          },
          count: {
            type: 'number'
          }
        }
      },
      ...baseResponseTemplate
    }
  },
  fn
};
