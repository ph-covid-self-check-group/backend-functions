import admin from '../../../utils/firebase';
import { verifyAdmin } from '../../../utils/auth/verify';

const db = admin.database();

/**
 *
 * @param {*} req
 * @param {*} res
 */
export const fn = async (req, res) => {
  const result = await verifyAdmin(req, res);

  if (!result) {
    return;
  }

  /**
   * @type {Array<any>}
   */
  const items = [];

  const snapshot = await db.ref('users/data').once('value');

  snapshot.forEach(snap => {
    console.log(snap.key);
    items.push({
      ...snap.val(),
      uid: snap.key
    });
  });

  return { success: true, items, count: items.length };
};
