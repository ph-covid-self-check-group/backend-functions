import { verifyAdmin } from '../../../utils/auth/verify';
import admin from '../../../utils/firebase';

const db = admin.database();

/**
 *
 * @param {*} req
 * @param {*} res
 */
export const fn = async (req, res) => {
  const { params } = req;
  const { uid } = params;

  const result = await verifyAdmin(req, res);

  if (!result) {
    return;
  }

  await db.ref(`/users/data/${uid}`).set(null);

  return { success: true };
};
