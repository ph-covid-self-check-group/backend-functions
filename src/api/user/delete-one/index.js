import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';
import { postResponseTemplate } from '../../../utils/response-templates/post-response-template';

export const del = {
  schema: {
    api: true,
    description: 'Delete one User object',
    tags: ['User'],
    summary: 'This method allows you to delete one User object',
    params: {
      type: 'object',
      properties: {
        uid: { type: 'string' }
      }
    },
    security: [
      {
        apiKey: []
      }
    ],
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' }
        }
      },
      ...baseResponseTemplate,
      ...postResponseTemplate
    }
  },
  fn
};
