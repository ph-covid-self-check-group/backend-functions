import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';

export const get = {
  schema: {
    api: true,
    description: 'Get one User object',
    tags: ['User'],
    summary: 'This method allows you to get one User object',
    params: {
      type: 'object',
      properties: {
        uid: { type: 'string' }
      }
    },
    security: [
      {
        apiKey: []
      }
    ],
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          data: 'UserUpdateResponse#'
        }
      },
      ...baseResponseTemplate
    }
  },
  fn
};
