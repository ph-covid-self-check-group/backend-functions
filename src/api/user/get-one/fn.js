
import admin from '../../../utils/firebase';
import { verifyAdmin } from '../../../utils/auth/verify';

const db = admin.database();

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply<Response>} res
 */
export const fn = async (req, res) => {
  const result = await verifyAdmin(req, res);

  if (!result) {
    return;
  }

  const { params } = req;
  const { uid } = params;

  const snapshot = await db.ref(`/users/data/${uid}`).once('value');

  if (!snapshot.exists) {
    return res
      .code(404)
      .send({
        success: false,
        code: 'data/not-found',
        message: 'The data you are trying to find cannot be found'
      });
  }

  return { success: true, data: { ...snapshot.val(), uid: snapshot.key } };
};
