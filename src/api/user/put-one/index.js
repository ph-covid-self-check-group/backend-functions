import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';
import { postResponseTemplate } from '../../../utils/response-templates/post-response-template';

export const put = {
  schema: {
    api: true,
    description: 'Update one User object',
    tags: ['User'],
    summary: 'This method allows you to update one User object',
    params: {
      type: 'object',
      properties: {
        uid: { type: 'string' }
      }
    },
    body: {
      type: 'object',
      properties: {
        data: 'UserUpdateRequest#'
      }
    },
    security: [
      {
        apiKey: []
      }
    ],
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          data: 'UserUpdateResponse#'
        }
      },
      ...baseResponseTemplate,
      ...postResponseTemplate
    }
  },
  fn
};
