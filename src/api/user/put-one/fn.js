import admin from '../../../utils/firebase';
import { verifyAdmin } from '../../../utils/auth/verify';

const db = admin.database();

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply<Response>} res
 */
export const fn = async (req, res) => {
  const result = await verifyAdmin(req, res);

  if (!result) {
    return;
  }

  const { params, body } = req;
  const { uid } = params;
  const { data } = body;

  const snap = await db.ref(`/users/data/${uid}`).once('value');

  if (!snap.exists) {
    if (!snap.exists) {
      return res
        .code(404)
        .send({
          success: false,
          code: 'data/not-found',
          message: 'The data you are trying to find cannot be found'
        });
    }
  }

  /**
   * @type {{ admin: boolean, fullname?: string }}
   */
  const updates = {
    admin: data.admin || false
  };

  if (data.fullname) {
    updates.fullname = data.fullname;
  }

  await db.ref(`/users/data/${uid}`).update(updates);

  return { success: true, data: { admin: body.admin, fullname: body.fullname } };
};
