import { fn } from './fn.js';
import { baseResponseTemplate } from '../../../utils/response-templates/base-response-template';
import { postResponseTemplate } from '../../../utils/response-templates/post-response-template';

export const put = {
  schema: {
    api: true,
    description: 'Updates a list of User objects',
    tags: ['User'],
    summary: 'This method allows you to update a list of User objects',
    body: {
      type: 'array',
      items: 'UserUpdateRequestMany#'
    },
    security: [
      {
        apiKey: []
      }
    ],
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' },
          items: {
            type: 'array',
            items: { $ref: 'UserUpdateResponse#' }
          }
        }
      },
      ...baseResponseTemplate,
      ...postResponseTemplate
    }
  },
  fn
};
