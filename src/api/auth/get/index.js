import { fn } from './fn.js';

export const get = {
  schema: {
    description: 'Authentication checker',
    tags: ['Auth'],
    summary: 'Authentication checker',
    security: [
      {
        apiKey: []
      }
    ],
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' }
        }
      }
    }
  },
  fn
};
