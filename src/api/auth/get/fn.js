import { verify } from '../../../utils/auth/verify';

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply<Response>} res
 */

export const fn = async (req, res) => {
  const result = await verify(req, res);
  return result;
};
