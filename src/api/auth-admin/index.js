import { get } from './get';

export const authAdmin = {
  '/auth/admin': {
    get
  }
};
