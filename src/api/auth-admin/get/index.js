import { fn } from './fn.js';

export const get = {
  schema: {
    description: 'Authentication Admin checker',
    tags: ['Auth'],
    summary: 'Authentication Admin checker',
    security: [
      {
        apiKey: []
      }
    ],
    response: {
      200: {
        description: 'Successful response',
        type: 'object',
        properties: {
          success: { type: 'boolean' }
        }
      }
    }
  },
  fn
};
