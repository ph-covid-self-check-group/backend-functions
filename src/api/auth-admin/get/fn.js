import { verifyAdmin } from '../../../utils/auth/verify';

/**
 *
 * @param {import('fastify').FastifyRequest} req
 * @param {import('fastify').FastifyReply<Response>} res
 */

export const fn = async (req, res) => {
  const result = await verifyAdmin(req, res);
  return result;
};
