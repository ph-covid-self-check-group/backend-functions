const setModel = require('../functions/set-model');
const apiRouteActions = require('./api-route-actions');
const cleanActions = require('./clean-actions');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

module.exports = [
  /**
   *
   * @param {PlopInquiry} data
   */
  function checker (data) {
    const { name } = data;
    if (!name.trim()) throw new Error('There\'s no name of the route/model to be added');
  },
  // get
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/get/fn.js',
    templateFile: 'plop-utils/templates/api/get-fn.hbs',
    abortOnFail: false
  },
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/get/index.js',
    templateFile: 'plop-utils/templates/api/get-index.hbs',
    abortOnFail: false
  },
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/get-one/fn.js',
    templateFile: 'plop-utils/templates/api/get-one-fn.hbs',
    abortOnFail: false
  },
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/get-one/index.js',
    templateFile: 'plop-utils/templates/api/get-one-index.hbs',
    abortOnFail: false
  },
  // update
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/put/fn.js',
    templateFile: 'plop-utils/templates/api/put-fn.hbs',
    abortOnFail: false
  },
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/put/index.js',
    templateFile: 'plop-utils/templates/api/put-index.hbs',
    abortOnFail: false
  },
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/put-one/fn.js',
    templateFile: 'plop-utils/templates/api/put-one-fn.hbs',
    abortOnFail: false
  },
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/put-one/index.js',
    templateFile: 'plop-utils/templates/api/put-one-index.hbs',
    abortOnFail: false
  },
  // delete
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/delete/fn.js',
    templateFile: 'plop-utils/templates/api/delete-fn.hbs',
    abortOnFail: false
  },
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/delete/index.js',
    templateFile: 'plop-utils/templates/api/delete-index.hbs',
    abortOnFail: false
  },
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/delete-one/fn.js',
    templateFile: 'plop-utils/templates/api/delete-one-fn.hbs',
    abortOnFail: false
  },
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/delete-one/index.js',
    templateFile: 'plop-utils/templates/api/delete-one-index.hbs',
    abortOnFail: false
  },
  // post
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/post/fn.js',
    templateFile: 'plop-utils/templates/api/post-fn.hbs',
    abortOnFail: false
  },
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/post/index.js',
    templateFile: 'plop-utils/templates/api/post-index.hbs',
    abortOnFail: false
  },
  // model
  {
    type: 'add',
    path: 'src/models/{{paramCase name}}.js',
    templateFile: 'plop-utils/templates/models/model.hbs',
    abortOnFail: false
  },
  {
    type: 'add',
    path: 'src/api/index.js',
    templateFile: 'plop-utils/templates/api/empty.hbs',
    abortOnFail: false
  },
  {
    type: 'add',
    path: 'src/api/{{paramCase name}}/index.js',
    templateFile: 'plop-utils/templates/api/empty.hbs',
    abortOnFail: false
  },
  {
    type: 'modify',
    path: 'src/models/{{paramCase name}}.js',
    transform: setModel
  },
  ...apiRouteActions,
  ...cleanActions,
  async function callSemiStandard () {
    console.log('Running semistandard to fix linting issues');
    const { stdout, stderr } = await exec('npx semistandard --fix');
    if (stdout) console.log('stdout', stdout);
    if (stderr) console.error('stderr', stderr);
  }
];
