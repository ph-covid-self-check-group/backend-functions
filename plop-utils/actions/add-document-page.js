module.exports = [
  {
    type: 'add',
    path: 'docs/{{path}}/{{docPathCreator}}',
    templateFile: 'plop-utils/templates/docs/page.hbs',
    abortOnFail: false
  }
];
