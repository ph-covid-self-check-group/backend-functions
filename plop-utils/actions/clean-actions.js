const setApiRoutes = require('../functions/set-api-routes');
const setModelRoutes = require('../functions/set-model-routes');

module.exports = [
  {
    type: 'modify',
    path: 'src/api/index.js',
    transform: setApiRoutes
  },
  {
    type: 'modify',
    path: 'src/models/index.js',
    transform: setModelRoutes
  }
];
