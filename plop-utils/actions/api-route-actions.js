const setApi = require('../functions/set-api');

module.exports = [
  {
    type: 'modify',
    path: 'src/api/{{paramCase name}}/index.js',
    transform: setApi
  }
];
