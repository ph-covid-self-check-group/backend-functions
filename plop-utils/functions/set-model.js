const { pascalCase } = require('change-case');

/**
 * @param {string} output
 * @param {PlopInquiry} data
 */
module.exports = (output, data) => {
  if (output) {
    console.log('There\'s already a model existing');
    return output;
  }
  const { name, modelProperties, uniqueId, uniqueIdType, uniqueIdFormat } = data;
  const pascalName = pascalCase(name);

  const props = Object.keys(modelProperties);
  const requiredProps = [];
  const nonNullableProps = [];

  /**
   * @type {ModelProperties}
   */
  const unique = {};

  unique[uniqueId] = {
    type: uniqueIdType
  };

  if (uniqueIdType === 'string' && uniqueIdFormat && uniqueIdFormat !== 'none') {
    unique[uniqueId].format = uniqueIdFormat;
  }

  for (const prop of props) {
    if (!modelProperties[prop].nullable) {
      nonNullableProps.push(prop);
    }

    if (modelProperties[prop].required) {
      requiredProps.push(prop);
    }
  }

  const model = {
    $id: pascalName,
    type: 'object',
    required: [uniqueId, ...props],
    properties: {
      ...unique,
      ...modelProperties
    }
  };

  const updateRequestModel = {
    $id: `${pascalName}UpdateRequest`,
    type: 'object',
    required: [],
    properties: {
      ...unique,
      ...modelProperties
    }
  };

  const updateResponseModel = {
    $id: `${pascalName}UpdateResponse`,
    type: 'object',
    required: [uniqueId, ...props],
    properties: {
      ...unique,
      ...modelProperties
    }
  };

  const postRequestModel = {
    $id: `${pascalName}CreateRequest`,
    type: 'object',
    required: [...requiredProps, ...nonNullableProps],
    properties: {
      ...modelProperties
    }
  };

  output = output + `export const ${pascalName} = ${JSON.stringify(model, null, 2).replace(/"/g, '\'')};\n\n`;
  output = output + `export const ${pascalName}UpdateRequest = ${JSON.stringify(updateRequestModel, null, 2).replace(/"/g, '\'')};\n\n`;
  output = output + `export const ${pascalName}UpdateResponse = ${JSON.stringify(updateResponseModel, null, 2).replace(/"/g, '\'')};\n\n`;
  output = output + `export const ${pascalName}CreateRequest = ${JSON.stringify(postRequestModel, null, 2).replace(/"/g, '\'')};\n\n`;
  output = output + `export default { ${pascalName}, ${pascalName}UpdateRequest, ${pascalName}UpdateResponse, ${pascalName}CreateRequest };\n`;
  return output;
};
