const pkg = require('../../package.json');

const pythonLicense = `#!/usr/bin/env python
#
# Copyright 2020, ${pkg.author}.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =========================================================================`;

const jsLicense = `/**
 * @license
 * Copyright 2020, ${pkg.author}.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */`;

const sassLicense = `///
/// @license
/// Copyright 2020, ${pkg.author}.
/// Licensed under the Apache License, Version 2.0 (the "License");
/// you may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
///    http://www.apache.org/licenses/LICENSE-2.0
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.
///
`;

/**
 * @return {String}
 */
module.exports = function () {
  // @ts-ignore
  const { path } = this;

  const pathArr = path.split('/');
  const file = pathArr[pathArr.length - 1];
  const fileArr = file.split('.');
  const fileType = fileArr[fileArr.length - 1];

  switch (fileType) {
    case 'py':
    case 'yml':
    case 'Dockerfile':
      return pythonLicense;
    case 'js':
    case 'ts':
    case 'styl':
      return jsLicense;
    case 'sass':
    case 'scss':
      return sassLicense;
    default:
      return '';
  }
};
