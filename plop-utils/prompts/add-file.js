// @ts-ignore
const isInvalidPath = require('is-invalid-path');

module.exports = [
  {
    type: 'input',
    name: 'path',
    message: 'Put the relative path of the file including the extension (e.g. path/to/the/file.txt)',
    /**
     * @param {String} value
     * @return {String | Boolean}
     */
    validate: (value) => {
      if ((/.+/).test(value) && !isInvalidPath(value)) { return true; }
      return 'Path is required';
    }
  }
];
