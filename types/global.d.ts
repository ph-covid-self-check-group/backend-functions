declare module 'chance';

interface SwaggerModelDefinition {
  [key: string]: {
    $id: string,
    type: string,
    required: Array<string>,
    properties: {
      [key: string]: {
        type: string,
        format?: string,
        nullable?: boolean
      }
    }
  }
}

interface PlopInquiry {
  name: string,
  modelProperties: ModelProperties,
  uniqueId: string,
  uniqueIdType: string,
  uniqueIdFormat: string
}

interface ModelProperties {
  [key: string]: {
    type: string,
    nullable?: boolean,
    required?: boolean,
    format?: string,
    properties?: ModelProperties
  }
}
